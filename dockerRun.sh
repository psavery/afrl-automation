#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage: <script> <inputFile>"
    exit 1
fi

inputFile=$1
workingDir=$(dirname $inputFile)
containerName=$(basename $workingDir)
echo 'inputFile is ' $inputFile
echo 'workingDir is ' $workingDir

# Run the smtk stuff
start=`date +%s.%N`
docker run --rm --entrypoint bash -v $PWD:/data -w /data psavery/smtk -c ". ~/setupEnvironment; python runner.py $inputFile"
end=`date +%s.%N`
smtkRunTime=$(echo "$end - $start" | bc)

if [ $? -ne 0 ]; then
    echo Error: SMTK docker run failed.
    exit 1
fi

echo "SMTK Docker Image runtime: $smtkRunTime seconds"

echo 'containerName is ' $containerName

# Finally, run albany
pushd .
cd $workingDir
start=`date +%s.%N`
docker run --rm --entrypoint /usr/local/albany/bin/AlbanyT -v $PWD:/data -w /data openchemistry/albany input.yaml
end=`date +%s.%N`
albanyRunTime=$(echo "$end - $start" | bc)
popd

totalRunTime=$(echo "$smtkRunTime + $albanyRunTime" | bc)

echo "====================================================="
echo "Timing information:"
echo "====================================================="
echo "SMTK Docker Image runtime: $smtkRunTime seconds"
echo "Albany Docker Image runtime: $albanyRunTime seconds"
echo "Total run time: $totalRunTime seconds"
echo "====================================================="
