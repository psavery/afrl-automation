#!/bin/bash

. ./setupEnvironment

echo 'Python in use:' $(which python)

pythonDir=./python
inputDir=./inputs
workingDir=./biggerCube/workingDir

mkdir -p $workingDir

transferDataSetScale=0.0031
transferDataSetTranslation="0.0 -6.3488 -6.3488"
transferDataSetRotation="0.0 0.0 0.0"
hardnessGenerationStyle="allHard"

input=$inputDir/20VF_MTR_Ellipsoids.xdmf
output=dream3d_cube_original.vti
python $pythonDir/Dream3DToVTK.py -i $input -m $workingDir/$output

if [ $? -ne 0 ]; then
    echo FAILED
    exit 1
fi

dreamMesh=$workingDir/$output

input=$inputDir/TestBar.exo
output=TestBar.vtu
python $pythonDir/ExodusToVTKConverter.py -i $input -m $workingDir/$output

if [ $? -ne 0 ]; then
    echo FAILED
    exit 1
fi

originalMesh=$workingDir/$output

input1=$originalMesh
input2=$dreamMesh
output=TestBar_classified.vtu
python $pythonDir/TransferDataSetProperties.py -c $transferDataSetScale \
                                               -t $transferDataSetTranslation \
                                               -r $transferDataSetRotation \
                                               -i $input1 -o $input2 \
                                               -s $workingDir/$output

if [ $? -ne 0 ]; then
    echo FAILED
    exit 1
fi

classifiedMesh=$workingDir/$output

input=$classifiedMesh
output=TestBar_contiguous.vtu
python $pythonDir/ContiguousDomains.py -i $input -s $workingDir/$output

if [ $? -ne 0 ]; then
    echo FAILED
    exit 1
fi

contiguousMesh=$workingDir/$output

input=$contiguousMesh
output=TestBar_BC.exo
python $pythonDir/ApplyElastomechanicalBoundaries_TestBar.py -i $input \
                                                        -s $workingDir/$output

if [ $? -ne 0 ]; then
    echo FAILED
    exit 1
fi

input=$workingDir/$output
albanyInputYamlFileName=input.yaml
albanyInputYaml=$workingDir/$albanyInputYamlFileName
elasticYaml=$workingDir/elastic.yaml
inputTemplate=$inputDir/inputTemplate.yaml
albanyOutputFileName=output.exo
python $pythonDir/GenerateAlbanyInputs.py -i $input -a $albanyInputYaml \
                                          -e $elasticYaml -t $inputTemplate \
                                          -o $albanyOutputFileName \
                                          -s $hardnessGenerationStyle

if [ $? -ne 0 ]; then
    echo FAILED
    exit 1
fi

# Finally, run albany
pushd .
cd $workingDir
#docker run --entrypoint /usr/local/albany/bin/AlbanyT -v $PWD:/data -w /data openchemistry/albany $albanyInputYamlFileName
popd
