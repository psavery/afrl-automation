
""" GenerateInputYaml.py:

For the AFRL Materials Phase II Demo, generates an input yaml
file for Albany input. This is called by GenerateAlbanyInputs.py

"""

# It would be nice if we could use pyyaml to do this, but I keep running
# into issues with albany not being able to read pyyaml output...
#
#import yaml
#
#def generateInputYamlStr(templateFile='inputTemplate.yaml'):
#    yaml.version = "1.1"
#    with open(templateFile, 'r') as stream:
#        try:
#            data = yaml.load(stream)
#        except yaml.YAMLError as exc:
#            print("Error reading template yaml:", templateFile)
#            print(exc)
#
#    print(data)
#
#    # Header
#    output =  '%YAML 1.1\n'
#    output += '---\n'
#
#    # The yaml contents
#    output += yaml.dump(data, default_flow_style=False)
#
#    # Footer
#    output += '...'
#
#    return output

from copy import deepcopy
from jinja2 import Template

def stringifyList(l):
    return '[' + ', '.join(str(x) for x in l) + ']'

def generateInputYamlStr(templateFile, templateVariables = {}):
    with open(templateFile, 'r') as rf:
        data = rf.read()

    tVars = deepcopy(templateVariables)

    # Stringify any lists
    for key in tVars.keys():
        if isinstance(tVars[key], list):
            tVars[key] = stringifyList(tVars[key])

    template = Template(data)
    output = template.render(**tVars)

    return output
