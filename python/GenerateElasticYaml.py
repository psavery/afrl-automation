
""" GenerateElasticYaml.py:

For the AFRL Materials Phase II Demo, generates an elastic yaml
file for Albany input. This is called by GenerateAlbanyInputs.py

"""

import random

# This takes a material dictionary and the original indentation
def generateMaterialModelStr(materialDict, indent):

    output =  indent + materialDict['materialName'] + ':\n'
    output += indent + '  Material Model:\n'
    output += indent + '    Model Name: ' + materialDict['modelName'] + '\n'
    output += indent + '  Elastic Modulus:\n'
    output += indent + '    Elastic Modulus Type: ' + materialDict['elasticModulusType'] + '\n'
    output += indent + '    Value: ' + str(materialDict['elasticModulusValue']) + '\n'
    output += indent + '  Poissons Ratio:\n'
    output += indent + '    Poissons Ratio Type: ' + materialDict['poissonsRatioType'] + '\n'
    output += indent + '    Value: ' + str(materialDict['poissonsRatioValue']) + '\n'
    output += indent + '  Thermal Conductivity:\n'
    output += indent + '    Thermal Conductivity Type: ' + materialDict['thermalConductivityType'] + '\n'
    output += indent + '    Value: ' + str(materialDict['thermalConductivityValue']) + '\n'
    output += indent + '  Thermal Transient Coefficient: ' + str(materialDict['thermalTransientCoefficient']) + '\n'
    output += indent + '  Thermal Expansion Coefficient: ' + str(materialDict['thermalExpansionCoefficient']) + '\n'
    output += indent + '  Density: ' + str(materialDict['density']) + '\n'

    return output


# Generate the elastic yaml file for Albany input. Returns a string with
# the file data in it.
# exodusReader should be a vtkExodusIIReader that already has the Exodus II
# file open
# hardnessGenerationStyle is a string that shows what kind of style should be
# used in the generation. Current options are allHard, allSoft, and random.
# materials should be a list of materials dictionaries (with keys for all the
# needed values)
def generateElasticYamlStr(exodusReader, materials,
                           hardnessGenerationStyle='allHard'):

    # The header
    yaml = '%YAML 1.1\n'
    yaml += '---\n'

    # Start the LCM section
    yaml += 'LCM:\n'

    indent = '  '
    # Start the elementBlocks section
    yaml += indent + 'ElementBlocks:\n'

    indent += '  '

    # Start reading from the exodus file
    numDomains = exodusReader.GetNumberOfElementBlockArrays()

    # First, figure out what the base material is. We assume it is the
    # one with the largest number of elements
    baseMatInd = 0
    bestNumElems = -1
    for i in range(numDomains):
        numElems = exodusReader.GetNumberOfEntriesInObject(1, i)
        if numElems > bestNumElems:
            baseMatInd = i
            bestNumElems = numElems

    if hardnessGenerationStyle == 'randomLabelled':
        swapPoint = numDomains / 2.0
        if numDomains == 3:
            # If we onlny have 3 domains, make sure that each one is
            # different.
            if baseMatInd == 0 or baseMatInd == 1:
                swapPoint = 1.5
            else:
                swapPoint == 0.5

    for i in range(numDomains):
        # These lines should all look like this:
        # Unnamed block ID: 105 Type: TETRA
        name = exodusReader.GetElementBlockArrayName(i)
        lineSplit = name.split()

        if len(lineSplit) != 6:
            print('Warning: unexpected block name:')
            print(name)
            continue

        blockId = lineSplit[3]

        if hardnessGenerationStyle == 'allHard':
            material = 'hardmat'
        elif hardnessGenerationStyle == 'allSoft':
            material = 'softmat'
        elif hardnessGenerationStyle == 'random':
            hard = bool(random.getrandbits(1))

            if hard:
              material = 'hardmat'
            else:
              material = 'softmat'
        elif hardnessGenerationStyle == 'randomLabelled':
            if i > swapPoint:
                material = 'softmat'
            else:
                material = 'hardmat'
        else:
            print 'Warning: unrecognized hardnessGenerationStyle:'
            print hardnessGenerationStyle
            print 'Defaulting to \'allHard\''
            material = 'hardmat'

        # But we will always use the base material for baseMatInd
        if i == baseMatInd:
            material = 'basemat'

        yaml += indent + 'Block_' + blockId + ':\n'
        yaml += indent + '  material: ' + material + '\n'

    indent = indent[:-2]
    yaml += indent + 'Materials:\n'
    indent += '  '

    # Generate the info for the materials
    for material in materials:
      yaml += generateMaterialModelStr(material, indent)

    # The footer
    yaml += '...\n'

    return yaml
