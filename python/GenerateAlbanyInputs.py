#!/usr/bin/env python

""" GenerateAlbanyInputs.py:

For the AFRL Materials Phase II Demo, takes the Exodus II file that has the
boundary conditions applied and generates Albany input files from it.

Hard and soft zones will be created.

"""

import os
import vtk

from jinja2 import Template

from GenerateElasticYaml import generateElasticYamlStr
from GenerateInputYaml import generateInputYamlStr

defaultMaterialSettings = {}
defaultMaterialSettings['materialName'] = 'basemat'
defaultMaterialSettings['modelName'] = 'Linear Elastic'
defaultMaterialSettings['elasticModulusType'] = 'Constant'
defaultMaterialSettings['elasticModulusValue'] = 3.0e6
defaultMaterialSettings['poissonsRatioType'] = 'Constant'
defaultMaterialSettings['poissonsRatioValue'] = 0.25
defaultMaterialSettings['thermalConductivityType'] = 'Constant'
defaultMaterialSettings['thermalConductivityValue'] = 5.0
defaultMaterialSettings['thermalTransientCoefficient'] = 1.0
defaultMaterialSettings['thermalExpansionCoefficient'] = 0.001
defaultMaterialSettings['density'] = 8.05e6


def applyDefaultSettingsToMaterials(materials):
    for material in materials:
        for key in defaultMaterialSettings.keys():
            if key not in material:
                material[key] = defaultMaterialSettings[key]

# 0 is the outer edge. It is constrained in X and Y, and allows time dependent
# DBC on Z.
# 1 is the inner edge. It is constrained in Z only.
# 2 is the side face. It is constrained in X only.
# 3 - n are the front faces. It is constrained in Y only.
def generateTurbineBCs(numNodes):
    numSpaces = 6
    indent = " " * numSpaces
    templateStr = indent
    templateStr += "DBC on NS nodelist_{{ nodeNum }} for DOF {{ DOF }}: "
    templateStr += "{{ force }}\n\n" # Jinja always deletes one newline

    zeroForce = "0.00000000e+00"

    template = Template(templateStr)
    output = "\n"
    output += template.render(nodeNum='0', DOF='X', force=zeroForce)
    output += template.render(nodeNum='0', DOF='Y', force=zeroForce)
    output += template.render(nodeNum='1', DOF='Z', force=zeroForce)
    output += template.render(nodeNum='2', DOF='X', force=zeroForce)

    # Apply the faces
    if numNodes > 3:
        for i in range(3, numNodes):
            output += template.render(nodeNum=str(i), DOF='Y', force=zeroForce)

    output += indent + "Time Dependent DBC on NS nodelist_0 for DOF Z:"

    return output


def generateAlbanyInputs(inputFile, albanyFile, elasticFile, inputTemplate,
                         albanyOutput, materials, hardnessGenerationStyle,
                         numNodes=0):
    exodusReader = vtk.vtkExodusIIReader()
    exodusReader.SetFileName(inputFile)
    exodusReader.Update()

    inputFileRoot = os.path.split(inputFile)[1]
    elasticFileRoot = os.path.split(elasticFile)[1]

    elasticYaml = generateElasticYamlStr(exodusReader, materials,
                                         hardnessGenerationStyle)

    templateVariables = {}
    templateVariables['inputFileName'] = inputFileRoot
    templateVariables['elasticFileName'] = elasticFileRoot
    templateVariables['outputFileName'] = albanyOutput

    if numNodes > 0:
        templateVariables['dirichletBCs'] = generateTurbineBCs(numNodes)
    else:
        # Check to see if numDirichlets.txt exists in the output directory
        # If it does, read that and use that.
        outputDir = os.path.dirname(albanyFile)
        fname = outputDir + '/numDirichlets.txt'
        if os.path.isfile(fname):
            with open(fname, 'r') as rf:
                numNodes = int(rf.read())
            if numNodes > 0:
                templateVariables['dirichletBCs'] = generateTurbineBCs(numNodes)

    templateVariables['timeValues'] = [0.000000, 1.000000]
    templateVariables['bcValues'] = [0.000000, 12.000000]

    inputYaml = generateInputYamlStr(templateFile=inputTemplate,
                                     templateVariables=templateVariables)

    with open(albanyFile, 'w') as wf:
        wf.write(inputYaml)

    with open(elasticFile, 'w') as wf:
        wf.write(elasticYaml)


if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-file',
                        help='<Required> Input Exodus II file',
                        required=True)

    parser.add_argument('-a', '--albany-file',
                        help='Output yaml file for Albany input',
                        default="input.yaml")

    parser.add_argument('-e', '--elastic-file',
                        help='Output elastic yaml file for Albany input',
                        default="elastic.yaml")

    parser.add_argument('-t', '--input-template',
                        help='The template file for Albany input yaml',
                        default="./inputs/inputTemplate.yaml")

    parser.add_argument('-o', '--albany-output',
                        help='Name of the output file for Albany',
                        default='output.exo')

    parser.add_argument('-s', '--hardness-generation-style',
                        help=("The generation style for the hardness. "
                              "Current accepted inputs are 'allHard', "
                              "'allSoft', and 'random'"),
                        default='allHard')

    args = parser.parse_args()

    materials = []

    # This will use all default settings
    basemat = {}
    basemat['materialName'] = 'basemat'
    materials.append(basemat)

    hardmat = {}
    hardmat['materialName'] = 'hardmat'
    hardmat['elasticModulusValue'] = 3.9e6
    materials.append(hardmat)

    softmat = {}
    softmat['materialName'] = 'softmat'
    softmat['elasticModulusValue'] = 2.1e6
    materials.append(softmat)

    applyDefaultSettingsToMaterials(materials)

    generateAlbanyInputs(args.input_file, args.albany_file, args.elastic_file,
                         args.input_template, args.albany_output, materials,
                         args.hardness_generation_style)
