#!/usr/bin/env python

""" ApplyThermalBoundaries.py:

For the AFRL Materials Case II Demo, takes the 3-dimensional mesh and constructs
boundary nodes for mechanical stress time-dependent Dirichlet boundaries. If a
domain cell data is present, the mesh is subdivided according to its annotation.

"""

import argparse
import math
import os
import random
import smtk
import smtk.io
import smtk.io.vtk
import smtk.mesh
import sys
import vtk

def readXMLFile(fileName):
    fn, fileExtension = os.path.splitext(fileName)
    if fileExtension == '.vtu':
        xmlReader = vtk.vtkXMLUnstructuredGridReader()
    elif fileExtension == '.vtp':
        xmlReader = vtk.vtkXMLPolyDataReader()
    xmlReader.SetFileName(fileName)
    xmlReader.Update()
    return xmlReader.GetOutputDataObject(0)

class OuterEdgeFilter(smtk.mesh.CellForEach) :
    def __init__(self, o, rmin):
        smtk.mesh.CellForEach.__init__(self, True)
        self.validPoints = list()
        self.origin = o
        self.rmin = rmin

    def forCell(self, meshHandle, cellType, numPoints):
        if numPoints < 3:
            return

        v0 = [0., 0., 0.] # unit vector from origin to first point in cell
        v1 = [0., 0., 0.] # unit vector from first point to second point in cell
        v2 = [0., 0., 0.] # unit vector from first point to third point in cell
        normal = [0., 0., 0.] # unit normal of cell
        length = [0., 0., 0.]

        # compute v0,v1,v2
        for i in range(0,3):
            v0[i] = self.coordinates()[i] - self.origin[i];
            length[0] += v0[i]*v0[i];
            v1[i] = self.coordinates()[3+i] - self.coordinates()[i];
            length[1] += v1[i]*v1[i];
            v2[i] = self.coordinates()[6+i] - self.coordinates()[i];
            length[2] += v2[i]*v2[i];

        length = map(math.sqrt, length)

        v0 = map(lambda x: x/length[0], v0)
        v1 = map(lambda x: x/length[1], v1)
        v2 = map(lambda x: x/length[2], v2)

        # compute normal
        for i in range(0,3):
            i1 = (i+1)%3
            i2 = (i+2)%3
            normal[i] = v1[i1]*v2[i2] - v1[i2]*v2[i1]

        mag = math.sqrt(normal[0]*normal[0] +
                        normal[1]*normal[1] +
                        normal[2]*normal[2]);

        normal = map(lambda x: x/mag, normal)

        # reject any cells whose normal is facing up or down
        if abs(normal[1]) > .005:
            return

        # reject any cells whose normal is facing out
        if abs(normal[0]) > .995:
            return

        # reject any cells whose normal is not facing outwards
        if abs(sum(i[0] * i[1] for i in zip(v0, normal))) < .95:
            return

        # reject any cells whose first coordinate is less than a distance <rmin>
        # from the axis of rotation
        for i in xrange(0,numPoints):
            r = math.sqrt(self.coordinates()[3*i]*self.coordinates()[3*i] +
                          self.coordinates()[3*i+2]*self.coordinates()[3*i+2])
            if r > self.rmin:
                self.validPoints.append(self.pointId(i))

class InnerEdgeFilter(smtk.mesh.CellForEach) :
    def __init__(self, o, rmax):
        smtk.mesh.CellForEach.__init__(self, True)
        self.validPoints = list()
        self.origin = o
        self.rmax = rmax

    def forCell(self, meshHandle, cellType, numPoints):
        if numPoints < 3:
            return

        v0 = [0., 0., 0.] # unit vector from origin to first point in cell
        v1 = [0., 0., 0.] # unit vector from first point to second point in cell
        v2 = [0., 0., 0.] # unit vector from first point to third point in cell
        centroid = [0., 0., 0.]
        normal = [0., 0., 0.] # unit normal of cell
        length = [0., 0., 0.]

        # compute v0,v1,v2
        for i in range(0,3):
            v0[i] = self.coordinates()[i] - self.origin[i];
            length[0] += v0[i]*v0[i];
            v1[i] = self.coordinates()[3+i] - self.coordinates()[i];
            length[1] += v1[i]*v1[i];
            v2[i] = self.coordinates()[6+i] - self.coordinates()[i];
            length[2] += v2[i]*v2[i];
            centroid[i] += self.coordinates()[i];

        length = map(math.sqrt, length)

        v0 = map(lambda x: x/length[0], v0)
        v1 = map(lambda x: x/length[1], v1)
        v2 = map(lambda x: x/length[2], v2)
        centroid = map(lambda x: x/3, centroid)

        # compute normal
        for i in range(0,3):
            i1 = (i+1)%3
            i2 = (i+2)%3
            normal[i] = v1[i1]*v2[i2] - v1[i2]*v2[i1]

        mag = math.sqrt(normal[0]*normal[0] +
                        normal[1]*normal[1] +
                        normal[2]*normal[2]);

        normal = map(lambda x: x/mag, normal)

        # reject any cells whose normal is facing up or down
        if abs(normal[1]) > .9:
            return

        # reject any cells whose normal is not facing inwards
        if abs(sum(i[0] * i[1] for i in zip(v0, normal))) > .9:
            return

        # reject any cells whose first coordinate is less than a distance <rmin>
        # from the axis of rotation
        for i in xrange(0,numPoints):
            r = math.sqrt(self.coordinates()[3*i]*self.coordinates()[3*i] +
                          self.coordinates()[3*i+2]*self.coordinates()[3*i+2])
            if r < self.rmax:
                self.validPoints.append(self.pointId(i))

class FrontFaceFilter(smtk.mesh.CellForEach) :
    def __init__(self, o, rmax):
        smtk.mesh.CellForEach.__init__(self, True)
        self.validPoints = list()
        self.origin = o
        self.rmax = rmax

    def forCell(self, meshHandle, cellType, numPoints):
        if numPoints < 3:
            return

        v0 = [0., 0., 0.] # unit vector from origin to first point in cell
        v1 = [0., 0., 0.] # unit vector from first point to second point in cell
        v2 = [0., 0., 0.] # unit vector from first point to third point in cell
        centroid = [0., 0., 0.]
        normal = [0., 0., 0.] # unit normal of cell
        length = [0., 0., 0.]

        # compute v0,v1,v2
        for i in range(0,3):
            v0[i] = self.coordinates()[i] - self.origin[i];
            length[0] += v0[i]*v0[i];
            v1[i] = self.coordinates()[3+i] - self.coordinates()[i];
            length[1] += v1[i]*v1[i];
            v2[i] = self.coordinates()[6+i] - self.coordinates()[i];
            length[2] += v2[i]*v2[i];
            centroid[i] += self.coordinates()[i];

        length = map(math.sqrt, length)

        v0 = map(lambda x: x/length[0], v0)
        v1 = map(lambda x: x/length[1], v1)
        v2 = map(lambda x: x/length[2], v2)
        centroid = map(lambda x: x/3, centroid)

        # compute normal
        for i in range(0,3):
            i1 = (i+1)%3
            i2 = (i+2)%3
            normal[i] = v1[i1]*v2[i2] - v1[i2]*v2[i1]

        mag = math.sqrt(normal[0]*normal[0] +
                        normal[1]*normal[1] +
                        normal[2]*normal[2]);

        normal = map(lambda x: x/mag, normal)

        if normal[1] < .5:
            return

        # reject any cells whose first coordinate is less than a distance <rmin>
        # from the axis of rotation
        for i in xrange(0,numPoints):
            r = math.sqrt(self.coordinates()[3*i]*self.coordinates()[3*i] +
                          self.coordinates()[3*i+2]*self.coordinates()[3*i+2])
            if r < self.rmax:
                self.validPoints.append(self.pointId(i))

class SideFaceFilter(smtk.mesh.CellForEach) :
    def __init__(self, o, rmax):
        smtk.mesh.CellForEach.__init__(self, True)
        self.validPoints = list()
        self.origin = o
        self.rmax = rmax

    def forCell(self, meshHandle, cellType, numPoints):
        if numPoints < 3:
            return

        v0 = [0., 0., 0.] # unit vector from origin to first point in cell
        v1 = [0., 0., 0.] # unit vector from first point to second point in cell
        v2 = [0., 0., 0.] # unit vector from first point to third point in cell
        centroid = [0., 0., 0.]
        normal = [0., 0., 0.] # unit normal of cell
        length = [0., 0., 0.]

        # compute v0,v1,v2
        for i in range(0,3):
            v0[i] = self.coordinates()[i] - self.origin[i];
            length[0] += v0[i]*v0[i];
            v1[i] = self.coordinates()[3+i] - self.coordinates()[i];
            length[1] += v1[i]*v1[i];
            v2[i] = self.coordinates()[6+i] - self.coordinates()[i];
            length[2] += v2[i]*v2[i];
            centroid[i] += self.coordinates()[i];

        length = map(math.sqrt, length)

        v0 = map(lambda x: x/length[0], v0)
        v1 = map(lambda x: x/length[1], v1)
        v2 = map(lambda x: x/length[2], v2)
        centroid = map(lambda x: x/3, centroid)

        # compute normal
        for i in range(0,3):
            i1 = (i+1)%3
            i2 = (i+2)%3
            normal[i] = v1[i1]*v2[i2] - v1[i2]*v2[i1]

        mag = math.sqrt(normal[0]*normal[0] +
                        normal[1]*normal[1] +
                        normal[2]*normal[2]);

        normal = map(lambda x: x/mag, normal)

        if abs(normal[1]) > 1.e-4:
            return

        if v0[0] < 0. or v1[0] < 0. or v2[0] < 0.:
            return

        if normal[0] < .05:
            return

       # reject any cells whose first coordinate is less than a distance <rmin>
       # from the axis of rotation
        for i in xrange(0,numPoints):
            r = math.sqrt(self.coordinates()[3*i]*self.coordinates()[3*i] +
                          self.coordinates()[3*i+1]*self.coordinates()[3*i+1] +
                          self.coordinates()[3*i+2]*self.coordinates()[3*i+2])
            if r < self.rmax:
                self.validPoints.append(self.pointId(i))

def labelIntersection(c, shell, filter_):
    shellCells = shell.cells();

    smtk.mesh.for_each(shellCells ,filter_)
    filteredCells = smtk.mesh.CellSet(c, filter_.validPoints)

#    if filteredCells.size() < 50:
#        return

    domains = c.domains()
    if len(domains) == 0:
        c.setDomainOnMeshes(c.meshes(), smtk.mesh.Domain(0))
        domains = c.domains()

    for dom in domains:
        domainMeshes = c.meshes(dom)

        domainCells = domainMeshes.cells()
        contactCells = smtk.mesh.point_intersect(
            domainCells, filteredCells, smtk.mesh.FullyContained)

        if not contactCells.is_empty():
            contactD = c.createMesh(contactCells)
            c.setDirichletOnMeshes(contactD,
                                   smtk.mesh.Dirichlet(
                                       labelIntersection.nextDirId))
            labelIntersection.nextDirId += 1

    return True
labelIntersection.nextDirId = 0

def breakMaterialsByCellType(c, randomizeDomainVals):
    domains = c.domains()

    domainVals = []
    if randomizeDomainVals:
        for i in range(len(domains)):
            domainVals.append(i)

        random.shuffle(domainVals)
    else:
        for dom in domains:
            domainVals.append(dom.value())

    domainsMade = 0
    for i, dom in enumerate(domains):
        domainMeshes = c.meshes(dom)

        for ct in xrange(smtk.mesh.Line, smtk.mesh.CellType_MAX):
            cells = domainMeshes.cells(smtk.mesh.CellType(ct))
            if not cells.is_empty():
                ms = c.createMesh(cells)
                v = (domainVals[i] * 100) + ct
                c.setDomainOnMeshes(ms, smtk.mesh.Domain(v))
                domainsMade += 1

        c.removeMeshes(domainMeshes)

def convert(inputFile, manager, material):
    cnvrt = smtk.io.vtk.ImportVTKData();
    collection = cnvrt( inputFile, manager, material );
    return collection

def extractMaterials(c, radius, frontFaceRMax, innerEdgeRMax, sideFaceRMax,
                     origin, outputFile, bounds, randomizeDomainVals):
    shell = c.meshes().extractShell()
    print ('There are %d shell mesh sets' % shell.size())

    ymin = bounds[2]
    ymax = bounds[3]
    center = [origin[0],origin[1] + (ymax+ymin)*.5,origin[2]]

    filter_ = OuterEdgeFilter(center,radius*2)
    labelIntersection(c, shell, filter_)

    filter_ = InnerEdgeFilter(center, innerEdgeRMax)
    labelIntersection(c, shell, filter_)

    filter_ = SideFaceFilter(center, sideFaceRMax)
    labelIntersection(c, shell, filter_)

    filter_ = FrontFaceFilter(center, frontFaceRMax)
    labelIntersection(c, shell, filter_)

#    print 'here:', len(c.domains())
    breakMaterialsByCellType(c, randomizeDomainVals)

    print ('number of domains: %d' % len(c.domains()))
    print ('number of dirichlets: %d' % len(c.dirichlets()))

    # Write the number of dirichlets to a file
    outputDir = os.path.dirname(outputFile)
    with open(outputDir + '/numDirichlets.txt', 'w') as wf:
        wf.write(str(len(c.dirichlets())))

    smtk.io.writeEntireCollection(outputFile, c)

def applyElastomechanicalBoundaries(inputFile, saveName, domainName,
                                    radius, frontFaceRMax, innerEdgeRMax,
                                    sideFaceRMax, origin, randomizeDomainVals):

    manager = smtk.mesh.Manager.create()

    c = convert(inputFile, manager, domainName)
    data = readXMLFile(inputFile)
    bounds = data.GetBounds()

    extractMaterials(c, radius, frontFaceRMax, innerEdgeRMax, sideFaceRMax,
                     origin, saveName, bounds, randomizeDomainVals)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i','--input-file',
                        help='<Required> Input data',
                        required=True)

    parser.add_argument('-s','--save-name',
                        help='name of output Exodus II file',
                        default='out.exo')

    parser.add_argument('-d','--domain-name',
                        help='name of the domain cell categorical data',
                        default='Domain')

    parser.add_argument('-r','--radius', type=float,
                        help='radius of the turbine blade',
                        default=30)

    parser.add_argument('-f', '--front-face-rmax', type=float,
                        help='max radius for front face filter',
                        default=52)

    parser.add_argument('-e', '--inner-edge-rmax', type=float,
                        help='max radius for the inner edge filter',
                        default=4.01)

    parser.add_argument('-m', '--side-face-rmax', type=float,
                        help='max radius for the side face filter',
                        default=53)

    parser.add_argument('-o','--origin', nargs=3, type=float,
                        help='origin',
                        default=[0, 0, 0])

    parser.add_argument('-v', '--randomize-domain-vals',
                        help='Randomize the domain values?', type=bool,
                        default=False)

    args = parser.parse_args()

    applyElastomechanicalBoundaries(args.input_file, args.save_name,
                                    args.domain_name, args.radius,
                                    args.front_face_rmax, args.inner_edge_rmax,
                                    args.side_face_rmax, args.origin,
                                    args.randomize_domain_vals)
