#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage: <script> <inputFile>"
    exit 1
fi

inputFile=$1
workingDir=$(dirname $inputFile)
echo 'inputFile is ' $inputFile
echo 'workingDir is ' $workingDir

. ./setupEnvironment

echo 'Python in use:' $(which python)

python ./runner.py $1

if [ $? -ne 0 ]; then
    echo Error: Python runner failed.
    exit 1
fi

# Finally, run albany
pushd .
cd $workingDir
#docker run --entrypoint /usr/local/albany/bin/AlbanyT -v $PWD:/data -w /data openchemistry/albany input.yaml
popd
